Pod::Spec.new do |spec|
  spec.name = 'boost'
  spec.version = '1.76.0'
  spec.license = { :type => 'Boost Software License', :file => "license.txt" }
  spec.homepage = 'http://www.boost.org'
  spec.summary = 'Boost provides free peer-reviewed portable C++ source libraries.'
  spec.authors = 'Mahmood Shahbazian'
  spec.source = { :git => 'https://gitlab.com/candleSoftware/boost.git' }
  # Pinning to the same version as React.podspec.
  spec.platforms = { :ios => '14.0', :tvos => '14.0' }
  spec.requires_arc = false

  spec.module_name = 'boost'
  spec.header_dir = './boost'
  spec.preserve_path = './boost'
end
